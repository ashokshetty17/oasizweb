
// $(".wb-input-group .form-control").on('focus blur', function(){
//     $(this).parent().toggleClass('is_focused');
// });

$(document).ready(function(){
$('.wb-input-group .form-control').on('blur change keyup', function(){
    inpValue = $(this).val();
    if(inpValue == '') {
        $(this).parent().addClass('empty');
        $(this).parent().removeClass('filled');
    } else {
        $(this).parent().addClass('filled');
        $(this).parent().removeClass('empty');
    }
});

setTimeout(function(){
    $('body').addClass('loaded');
    $('.preloader').fadeOut(1000);
}, 3700);

//scroll sections
var navbarHeight = $('.navbar').height() - 30;  // get height of fixed navbar

$(".scroll").on('click', function(event) {
    event.preventDefault();
    $('html,body').animate({
        scrollTop: $(this.hash).offset().top - navbarHeight
    }, 900, 'easeInOutExpo');
});

// Closes responsive menu when a scroll trigger link is clicked
$('.scroll').click(function () {
    $('.navbar-collapse').collapse('hide');
});

// Activate scrollspy to add active class to navbar items on scroll
// $('body').scrollspy({
//     target: '.wb-nav',
//     offset: 50
// });

/* -------------------------------------------------------------
    nav js
------------------------------------------------------------- */
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".wb-nav").addClass("isScrolled");
    } else {
        $(".wb-nav").removeClass("isScrolled");
    }
});


$('.wb-book-bubble-btn').click( function(){
    $('.wb-book-bubble-modal, .wb-book-bubble-modal-overlay').addClass('active');
    // $('body').addClass('hidden');
});

$('.wb-book-bubble-modal-close, .wb-book-bubble-modal-overlay').click( function() {
    $('.wb-book-bubble-modal, .wb-book-bubble-modal-overlay').removeClass('active');
    // $('body').removeClass('hidden');    
});


// $('.counter').counterUp({
//     delay: 10,
//     time: 1500
// });

});
