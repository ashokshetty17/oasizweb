
import React from 'react'
import { hashHistory } from 'react-router'
import './login.scss'
import APICallManager from '../../../services/callmanager'
import config from '../../../../public/config.json'

class EndUsersLoginOTPValidation extends React.Component {
  constructor () {
    super()
    this.state = {
      otpNumber: '',
      loginMobile: localStorage.getItem('loginMobile'),
      errorMessage: '',
      loading: true,
      authObj: JSON.parse(localStorage.getItem('authObj'))
    }

    this.handleEndUserLoginOTP = this.handleEndUserLoginOTP.bind(this)
    this.handleOTPNumKeys = this.handleOTPNumKeys.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleEndUserLoginOTP (event) {
    this.setState({ otpNumber: event.target.value, errorMessage: '' })
  }

  handleOTPNumKeys (event) {
    if ((event.charCode > 31 && event.charCode < 48) ||
    (event.charCode > 57 && event.charCode < 127)) {
      event.preventDefault()
    }
  }
  componentDidMount () {
    setTimeout(() => {
      this.setState({ loading: false })
    }, 500)
  }

  handleSubmit (event) {
    const otpNumValid = /^\d{4}$/
    let otpNum = this.state.otpNumber
    if (!otpNum) {
      this.setState({ errorMessage: 'OTP is required' })
    } else if (!otpNumValid.test(otpNum)) {
      this.setState({ errorMessage: 'Inavlid OTP Number' })
    } else {
      this.setState({ errorMessage: '' })
      let postObj = {
        url: config.baseUrl + config.postEndUserMobileRegOTPValidationAPI,
        body: { otpNumber: otpNum }
      }
      let isToken = 'true'
      let _this = this
      APICallManager.postCall(postObj, isToken, function (resObj) {
        try {
          if (resObj.data.statusResult && resObj.data.statusResult.userAccount) {
            localStorage.removeItem('loginMobile')
            localStorage.setItem('authObj', JSON.stringify(resObj.data.statusResult))
            if (resObj.data.statusResult.custLocationObj && resObj.data.statusResult.custLocationObj.area) {
              localStorage.setItem('address', JSON.stringify(resObj.data.statusResult.custLocationObj))
              hashHistory.push('/orders')
            } else {
              hashHistory.push('/locations/set')
            }
          } else {
            _this.setState({ errorMessage: 'Invalid OTP Number' })
          }
        } catch (e) {
          console.log('=====ERROR:', e)
        }
      })
    }
    event.preventDefault()
  }

  render () {
    let content = this.state.loading
      ? <div id='app' className='loader1' style={{ textAlign:'center', padding:50 }}>
        <b>please wait...</b>
        <br />
        <i className='fa fa-spinner fa-spin' style={{ fontSize:24 }} />
      </div>
      : <div className='app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden'>
          <nav>
            <header className='app-header navbar'>
              <div className='container'>
                <a className='navbar-brand' href='/'>
                  {/* <img src={require('../../../images/oasiz-logo-white.png')} width={150} height={48} /> */}
                </a>
              </div>
            </header>
          </nav>
        <div className='' id='top'>
          {/* <nav className='navbar navbar-expand-md navbar-light navbar-dark fixed-top wb-nav'>
            <a className='navbar-brand' href='/'>
              <img src={require('../../../supplier-images/logo.png')} width={150} height={48} />
            </a>
          </nav> */}
          {/* _____________________________ navigation End _______________________________ */}
          <main className='main-content enduser_loginpage'>
            <section className='wb-section ' id='products'>
              <div className='form-body'>
                <div className='website-logo'>
                  {/* <div className='logo'>
                    <img className='logo-size' src={require('../../../images/oasiz-logo12.png')} />
                  </div> */}
                </div>
                <div className='img-holder'>
                  <div className='bg'>{/* ---div Style--- */}</div>
                  <div className='info-holder'>
                    <img className='logo-size' src={('http://brandio.io/envato/iofrm/html/images/graphic2.svg')} />
                  </div>
                </div>
                <div className='form-holder'>
                  <div className='form-content'>
                    <div className='inner-box form-items'>
                      <div className='page-links'><h2 className='title-2 uppercase'>Enter OTP Number</h2></div>
                      <form onSubmit={this.handleSubmit} className='otp-form'>
                        <div className='otp_div'>
                          <p>Please enter the OTP sent to <strong> {this.state.loginMobile} </strong> </p>
                          <div className='input-group mb-4'>
                            <div className='otp_field'>
                              <div className='input-group-prepend'> <span className='input-group-text'> <i className='fa fa-shield' style={{ color:'#66bdb0' }} /> </span>
                                <input autoFocus type='text' className='form-control' placeholder='OTP' autoComplete='off' maxLength='4' name='endusers-loginotp-validation-name' id='endusers-loginotp-validation-id'
                                  value={this.state.otpNumber} onChange={this.handleEndUserLoginOTP} onKeyPress={this.handleOTPNumKeys} />
                              </div>
                            </div>
                          </div>
                          <button type='submit' className='btn orange' >Verify</button>
                          <div className='col-sm-12'>
                            <label className='warning' >{this.state.errorMessage}</label>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </main>
        </div>
      </div>
    return (
      <div>{content}</div>
    )
  }
}

export default EndUsersLoginOTPValidation
