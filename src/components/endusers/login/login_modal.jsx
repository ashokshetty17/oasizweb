import React, { Component } from 'react'
import { hashHistory } from 'react-router'

class LoginModal extends Component {
  constructor () {
    super()
    this.state = {
      modalIsOpen: false
    }
    this.handleLogin = this.handleLogin.bind(this)
  }
  handleLogin () {
    hashHistory.push('/login')
  }
  render () {
    return (
      <div>
        <li className='lang-menu nav-item'>
          <button onClick={this.handleLogin} className='btn' data-toggle='modal' data-target='#reservationModal'><i className='icon-lock'>{/* ---ICON--- */}</i> Login / Register </button>
          {/* <button className='  btn btn-default' data-toggle='modal' data-target='#reservationModal'>Register&nbsp; <i className='icon-user' /></button> */}
        </li>
      </div>
    )
  }
}

export default LoginModal
