import React from 'react'
import { hashHistory } from 'react-router'

import './login.scss'
import APICallManager from '../../../services/callmanager'
import config from '../../../../public/config.json'

class EndUsersLogin extends React.Component {
  constructor () {
    super()
    this.state = {
      name: '',
      mobileNum: '',
      mobileNumber: '',
      errorMessage: '',
      userError:'',
      userSuccess:'',
      mobileError:'',
      mobileSuccess:'',
      loading: true,
      authObj: JSON.parse(localStorage.getItem('authObj'))
    }

    this.handleEndUserLoginChange = this.handleEndUserLoginChange.bind(this)
    this.handleMobileNumKeys = this.handleMobileNumKeys.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.endUserSignUp = this.endUserSignUp.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
    this.handleBlurMobile = this.handleBlurMobile.bind(this)
    this.handleBlurSubmit = this.handleBlurSubmit.bind(this)
  }
  componentDidMount () {
    setTimeout(() => {
      this.setState({ loading: false })
    }, 500)
  }
  endUserSignUp (event) {
    hashHistory.push('/signup')
    event.preventDefault()
  }
  handleBlur () {
    if (this.state.name === '') {
      this.setState({
        userError:'User Name is required',
        userSuccess:''
      })
    } else { this.setState({ userError:'', userSuccess:'success' }) }
  }
  handleBlurMobile () {
    if (this.state.mobileNum === '') {
      this.setState({
        mobileError:'Mobile Number required',
        mobileSuccess:''
      })
    } else { this.setState({ mobileError:'', mobileSuccess:'success' }) }
  }
  handleBlurSubmit () {
    if (this.state.name === '') {
      this.setState({
        userError:'User Name is required',
        userSuccess:''
      })
    } else if (this.state.mobileNum === '') {
      this.setState({
        mobileError:'Mobile Number required',
        mobileSuccess:''
      })
    }
  }
  handleEndUserLoginChange (event) {
    this.setState({ userError: '', mobileError: '' })
    if (event.target.id === 'endusers-loginuser-mobilenum-id') {
      let num = event.target.value
      this.setState({ mobileNum: event.target.value, errorMessage: '' })
      if (num) {
        this.setState({ mobileNumber: num.match(/\d+/g).toString().replace(/,/g, '') })
      }
    } else if (event.target.id === 'endusers-loginuser-name-id') {
      this.setState({ name: event.target.value, errorMessage: '' })
    }
  }

  handleMobileNumKeys (event) {
    if ((event.charCode >= 32 && event.charCode < 48 && event.charCode !== 40 &&
    event.charCode !== 41 && event.charCode !== 43 && event.charCode !== 45) ||
    (event.charCode > 57 && event.charCode < 127)) {
      event.preventDefault()
    }
  }

  handleNameNumKeys (event) {
    if (event.charCode === 32) {
      event.preventDefault()
    }
  }

  handleSubmit (event) {
    // const phValidation = /^((\(\d{3}\))|\d{3,4})(\-|\s)?(\d{3})(\-|\s)?(\d{1,4})(\-|\s)?(\d{1,3})$/
    if (!this.state.name.trim()) {
      this.setState({ errorMessage: 'Name is required' })
    } else if (!this.state.mobileNum) {
      this.setState({ errorMessage: 'Mobile Number is required' })
    } else {
      if (this.state.authObj && this.state.authObj.entityType && this.state.authObj.entityType === 'Admin') {
        this.setState({ errorMessage: 'Already logged in  as Admin' })
      } else if (this.state.authObj && this.state.authObj.entityType && this.state.authObj.entityType === 'Service Provider') {
        this.setState({ errorMessage: 'Already logged in  as Service Provider' })
      // } else {
      // if (this.state.authObj && this.state.authObj.entityType === 'Service Provider') {
      //   this.setState({ errorMessage: 'Already logged in  as Service Provider' })
      } else {
        this.setState({ errorMessage: '' })
        let postObj = {
          url: config.baseUrl + config.postEndUserMobileRegOTPSendAPI,
          body: { name: this.state.name, mobileNumber: this.state.mobileNumber }
        }
        let isToken = 'true'
        let _this = this
        APICallManager.postCall(postObj, isToken, function (resObj) {
          try {
            console.log('OTP===', resObj.data.statusResult)
            localStorage.setItem('loginMobile', _this.state.mobileNum)
            hashHistory.push('/login/otp')
          } catch (e) {
            console.log('=====ERROR:', e)
          }
        })
      }
    }
    event.preventDefault()
  }

  render () {
    let content = this.state.loading
      ? <div id='app' className='loader1' style={{ textAlign:'center', padding:50 }}>
        <b>please wait...</b>
        <br />
        <i className='fa fa-spinner fa-spin' style={{ fontSize:24 }} />
      </div>
      : <div>
        <div className='app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden'>
        <nav>
          <header className='app-header navbar'>
            <div className='container'>
              <a className='navbar-brand' href='/'>
                {/* <img src={require('../../../images/oasiz-logo-white.png')} width={150} height={48} /> */}
              </a>
            </div>
          </header>
        </nav>
          <div className='' id='top'>
            {/* <nav className='navbar navbar-expand-md navbar-light navbar-dark fixed-top wb-nav'>
              <a className='navbar-brand' href='/'>
                <img src={require('../../../supplier-images/logo.png')} width={150} height={48} />
              </a>
            </nav> */}
            {/* _____________________________ navigation End _______________________________ */}
            <main className='main-content enduser_loginpage'>
              <section className='wb-section' id='products'>
                <div className='form-body'>
                  <div className='website-logo'>
                    {/* <div className='logo'>
                      <img className='logo-size' src={require('../../../images/oasiz-logo12.png')} />
                    </div> */}
                  </div>
                  <div className='img-holder'>
                    <div className='bg' />
                    <div className='info-holder'>
                      <img className='logo-size' src={('http://brandio.io/envato/iofrm/html/images/graphic2.svg')} />
                    </div>
                  </div>
                  <div className='form-holder'>
                    <div className='form-content'>
                      <div className='inner-box form-items'>
                        <div className='page-links'><h2 className='title-2 uppercase'>EndUser - Login</h2></div>
                        <form className='login'>
                          <div className='input-group mb-3 mb-0'>
                            <div className='input-group-prepend'> <span className='input-group-text'> <i className='icon-user'>{/* ---Icon--- */}</i> </span> </div>
                            <input type='text' autoFocus autoComplete='off' name='endusers-loginuser-name-name' id='endusers-loginuser-name-id' maxLength='40' placeholder='Enter Username'
                              value={this.state.name} onChange={this.handleEndUserLoginChange} onBlur={this.handleBlur} className='form-control' required onKeyPress={this.handleNameNumKeys}
                              style={this.state.userError ? { display:null, borderColor:'red', borderWidth:1 } : { color:'@eee' }}
                              />
                            <div >
                              <i className='fa fa-check ' style={this.state.userSuccess ? { display:null, color: 'green', position:'relative', marginLeft:-25, top:6, zIndex:2, fontSize:20 }
                                : { display : 'none', color:'green', position:'relative', left:-23 }} />
                              <i className='fa fa-times-circle-o ' style={this.state.userError ? { display:null, color: 'red', position:'relative', marginLeft:-25, top:6, zIndex:2, fontSize:20 }
                                : { display : 'none', color:'red', position:'relative', left:-35 }} />
                              <i className='fa fa-exclamation-circle 'style={{ display : 'none', color:'orange', position:'relative', marginLeft:-25, top:6, zIndex:2, fontSize:20 }} />
                            </div>
                          </div>
                          <div>
                            <label style={{ color:'red', marginBottom:0, fontWeight:400, fontSize:10 }} >{this.state.userError}</label>
                          </div>
                          <div className='input-group mb-3 mb-0'>
                            <div className='input-group-prepend'> <span className='input-group-text'> <i className='icon-lock'>{/* ---Icon--- */}</i> </span> </div>
                            <input autoComplete='off' name='endusers-loginuser-mobilenum-name' id='endusers-loginuser-mobilenum-id' maxLength='10' pattern='\d{10}'
                              value={this.state.mobileNum} onChange={this.handleEndUserLoginChange} onBlur={this.handleBlurMobile} onKeyPress={this.handleMobileNumKeys} placeholder='Mobile Number'
                              className='form-control' type='text' required style={this.state.mobileError ? { display:null, borderColor:'red', borderWidth:1 } : { color:'@eee' }}
                              />
                            <div >
                              <i className='fa fa-check ' style={this.state.mobileSuccess ? { display:null, color: 'green', position:'relative', marginLeft:-25, top:6, zIndex:2, fontSize:20 }
                                : { display : 'none', color:'green', position:'relative', left:-23 }} />
                              <i className='fa fa-times-circle-o ' style={this.state.mobileError ? { display:null, color: 'red', position:'relative', marginLeft:-25, top:6, zIndex:2, fontSize:20 }
                                : { display : 'none', color:'red', position:'relative', left:-35 }} />
                              <i className='fa fa-exclamation-circle 'style={{ display : 'none', color:'orange', position:'relative', marginLeft:-25, top:6, zIndex:2, fontSize:20 }} />
                            </div>
                          </div>
                          <div>
                            <label style={{ color:'red', marginBottom:0, fontWeight:400, fontSize:10 }} >{this.state.mobileError}</label>
                          </div>
                          <div className='row'>
                            <div className='col-sm-4 '>
                              <button type='button' onClick={this.handleSubmit} className='btn orange ' onBlur={this.handleBlurSubmit} >Submit</button>
                            </div>
                            <div className='col-sm-8 '>
                              <label className='warning'>{this.state.errorMessage}</label>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </main>
          </div>
        </div>
      </div>
    return (
      <div>{content}</div>
    )
  }
}

export default EndUsersLogin
