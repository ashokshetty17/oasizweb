import React, { Component } from 'react'
import { FormControl, Button } from 'react-bootstrap'
import { hashHistory } from 'react-router'

import '../login/login.scss'
import Profile from '../Profile'
import LoginModal from '../login/login_modal'
import APICallManager from '../../../services/callmanager'
import locations from '../orders/locations.json'
import config from '../../../../public/config.json'
import { fetch as fetchPolyfill } from 'whatwg-fetch'

class EndUsersSetLocation extends Component {
  constructor () {
    super()
    this.state = {
      latitude: 0,
      longitude: 0,
      locationName: 'Home',
      houseNumber: '',
      street: '',
      landmark: '',
      area: '',
      areaLocality: '',
      zip: '',
      city: 'Hyderabad',
      state: 'Telangana',
      errorMessage: '',
      authObj: JSON.parse(localStorage.getItem('authObj')),
      customerLongitude: 0,
      customerLatitude: 0
    }
    this.getAddress = this.getAddress.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleLocationChange = this.handleLocationChange.bind(this)
    this.handlePlotChange = this.handlePlotChange.bind(this)
    this.handleStreetChange = this.handleStreetChange.bind(this)
    this.handleLandmarkChange = this.handleLandmarkChange.bind(this)
    this.handleAreaChange = this.handleAreaChange.bind(this)
    this.handleLocalityChange = this.handleLocalityChange.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }
  componentDidMount () {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null
        })
        this.getAddressComponents(position.coords.latitude, position.coords.longitude)
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
    )
  }
  getAddressComponents (lat, long) {
    fetchPolyfill(config.googleGeoLocationLatLng + lat + ',' + long + '&key=' + config.googleMapsAPIKey)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ area: responseJson.results[0].formatted_address.split(',')[0] + ',' + responseJson.results[0].formatted_address.split(',')[1] })
        responseJson.results[0].address_components.forEach(data => {
          if (data.types.indexOf('premise') !== -1) {
            this.setState({ houseNumber: data.long_name })
          }
          if (data.types.indexOf('sublocality_level_2') !== -1) {
            this.setState({ street: data.long_name })
          }
          if (data.types.indexOf('sublocality_level_1') !== -1) {
            this.setState({ area: data.long_name })
          }
          if (data.types.indexOf('postal_code') !== -1) {
            this.setState({ zip: data.long_name })
          }
          if (data.types.indexOf('locality') !== -1) {
            this.setState({ areaLocality: data.long_name })
          }
          if (data.types.indexOf('administrative_area_level_2') !== -1) {
            this.setState({ city: 'Hyderabad' })
          }
          if (data.types.indexOf('administrative_area_level_1') !== -1) {
            this.setState({ state: 'Telangana' })
          }
        })
      }).catch((error) => {
        console.log('fetch error: ' + error)
      })
    this.setState({ authObj: JSON.parse(localStorage.getItem('authObj')) })
  }
  getAddress () {
    this.getAddressComponents(this.state.latitude, this.state.longitude)
  }

  handleLocationChange (event) {
    this.setState({
      locationName: event.target.value,
      errorMessage: ''
    })
  }
  handlePlotChange (event) {
    this.setState({
      houseNumber: event.target.value,
      errorMessage: ''
    })
  }
  handleStreetChange (event) {
    this.setState({
      street: event.target.value,
      errorMessage: ''
    })
  }
  handleLandmarkChange (event) {
    this.setState({
      landmark: event.target.value,
      errorMessage: ''
    })
  }
  handleAreaChange (event) {
    this.setState({
      area: event.target.value,
      errorMessage: ''
    })
  }
  handleLocalityChange (event) {
    this.setState({
      areaLocality: event.target.value,
      errorMessage: ''
    })
  }
  handleNameNumKeys (event) {
    if (event.charCode === 32) {
      event.preventDefault()
    }
  }
  handleChange (event) {
    switch (event.target.name) {
      case 'endusers-setlocation-zipcode-name':
        if (event.target.value.length <= 6) {
          this.setState({ zip: event.target.value, errorMessage: '' })
        }
        break
      case 'endusers-setlocation-locationname-name':
        this.setState({ locationName: event.target.value, errorMessage: '' })
        break
      case 'endusers-setlocation-city-name':
        this.setState({ city: event.target.value, errorMessage: '' })
        break
      case 'endusers-setlocation-state-name':
        this.setState({ state: event.target.value, errorMessage: '' })
        break
    }
  }

  handleUpdate (event) {
    if (!this.state.locationName.trim()) {
      this.setState({ errorMessage: 'Location Name is required' })
    } else if (!this.state.houseNumber.trim()) {
      this.setState({ errorMessage: 'Plot/House Number is required' })
    } else if (!this.state.street.trim()) {
      this.setState({ errorMessage: 'Street Name is required' })
    } else if (!this.state.landmark.trim()) {
      this.setState({ errorMessage: 'Landmark is required' })
    } else if (!this.state.area) {
      this.setState({ errorMessage: 'Area is required' })
    } else if (!this.state.city.trim()) {
      this.setState({ errorMessage: 'City is required' })
    } else if (!this.state.state.trim()) {
      this.setState({ errorMessage: 'State is required' })
    } else if (!this.state.areaLocality.trim()) {
      this.setState({ errorMessage: 'AreaLocality is required' })
    } else if (!this.state.zip.trim()) {
      this.setState({ errorMessage: 'Pin Code is required' })
    } else {
      this.sendRequest()
    }
    event.preventDefault()
  }

  /* Browser Back Button Handler */
  componentDidUpdate () {
    /* window.onpopstate = (e) => {
      console.log('Browser Back Button Handler')
    } */
  }

  sendRequest () {
    var addressLocation = this.state.plotNumber + ',' + this.state.street + ',' + this.state.area + ',' + this.state.city + ',' + this.state.state + ',' + this.state.zip
    let _this = this
    fetchPolyfill(config.googleGeoLocationAddress + addressLocation + 'IN&key=' + config.googleMapsAPIKey)
    .then((response) => response.json())
    .then((responseJson) => {
      if (_this.state.authObj && _this.state.authObj.entityType === 'Customer') {
        let locData = {
          customer: (_this.state.authObj.customerObj && _this.state.authObj.customerObj.name) ? _this.state.authObj.customerObj.name : '',
          locationName: _this.state.locationName,
          houseNumber: _this.state.houseNumber,
          address: _this.state.street,
          landmark: _this.state.landmark,
          area: _this.state.area,
          areaLocality: _this.state.areaLocality,
          zip: _this.state.zip,
          city: _this.state.city,
          state: _this.state.state,
          customerLatitude: (responseJson.results && responseJson.results.length > 0) ? responseJson.results[0].geometry.location.lat : 0,
          customerLongitude: (responseJson.results && responseJson.results.length > 0) ? responseJson.results[0].geometry.location.lng : 0
        }
        let obj = { url: config.baseUrl + config.postEndUserCreateLocationAPI, body: locData }
        let isToken = this.state.authObj && this.state.authObj.entityType === 'Customer' ? 'true' : 'false'
        APICallManager.postCall(obj, isToken, function (res) {
          if (res.data.statusCode === '1050') {
            localStorage.setItem('address', JSON.stringify(res.data.statusResult))
            hashHistory.push('/orders')
          } else {
            _this.setState({
              errorMessage: 'location created failed'
            })
          }
        })
      } else {
        let address = {
          locationName: _this.state.locationName ? _this.state.locationName : 'Home',
          houseNumber: _this.state.houseNumber,
          address: _this.state.street,
          landmark: _this.state.landmark,
          area: _this.state.area,
          areaLocality: _this.state.areaLocality,
          zip: _this.state.zip,
          city: _this.state.city,
          state: _this.state.state,
          customerLatitude: (responseJson.results && responseJson.results.length > 0) ? responseJson.results[0].geometry.location.lat : 0,
          customerLongitude: (responseJson.results && responseJson.results.length > 0) ? responseJson.results[0].geometry.location.lng : 0
        }
        localStorage.setItem('address', JSON.stringify(address))
        hashHistory.push('/orders')
      }
    })
  }

  render () {
    return (
      <div className='app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden'>
        <nav>
          <header className='app-header navbar'>
            <div className='container'>
              <a className='navbar-brand' href='/'>
                {/* <img src={require('../../../images/oasiz-logo-white.png')} width={150} height={48} /> */}
              </a>
              <div className='nav navbar-nav ml-auto navbar-right navbar-nav-mobile'>
                {(this.state.authObj && this.state.authObj !== 'undefined' && this.state.authObj.userAccount && this.state.authObj.entityType === 'Customer')
                  ? <Profile />
                  : <LoginModal />}
                {/* <LoginModal/>  */}
              </div>{/* /.wb-book-bubble-box */}
            </div>
          </header>
        </nav>
        <div className='main-content enduser_setlocation'>
          <section className='wb-section' id='products'>
            <div className='form-body'>
              <div className=''>
                <div className='img-holder' style={{ padding: 20 }}>
                  <div className='bg' />
                  <div className='info-holder'>
                    <div className='col-md-12'>
                      <iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d487295.025366022!2d78.12783906221374!3d17.412153058595425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb99daeaebd2c7%3A0xae93b78392bafbc2!2sHyderabad%2C+Telangana!5e0!3m2!1sen!2sin!4v1535553880507' width='100%' height='450' />
                    </div>
                  </div>
                </div>
                <div className='form-holder'>
                  <div className='form-content'>
                    <div className='inner-box form-items'>
                      <h2 className='title-2 uppercase'><strong> Set Address Location</strong></h2>
                      <div className='row'>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='select-style'>
                              <select className='form-control' name='endusers-setlocation-locationname-name' value={this.state.locationName} onChange={this.handleChange}>
                                <option value='Home'>Home</option>
                                <option value='Office'>Office</option>
                                <option value='Other'>Other</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='input-group'>
                              <span className='input-group-addon'><i className='fa fa-building' aria-hidden='true' /></span>
                              <FormControl type='text' value={this.state.houseNumber} placeholder='Plot/House #' onChange={this.handlePlotChange} minLength={3} maxLength={15} /></div></div></div>
                      </div>

                      <div className='row'>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='input-group'>
                              <span className='input-group-addon'><i className='fa fa-street-view' aria-hidden='true' /></span>
                              <FormControl type='text' value={this.state.street} placeholder='Street Name' onChange={this.handleStreetChange} minLength={3} maxLength={30} /></div></div></div>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='input-group'>
                              <span className='input-group-addon'><i className='fa fa-map-signs' aria-hidden='true' /></span>
                              <FormControl type='text' value={this.state.landmark} placeholder='Landmark' onChange={this.handleLandmarkChange} minLength={3} maxLength={30} /></div></div></div>
                      </div>
                      <div className='row'>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='select-style'>
                              <select className='form-control' name='endusers-setlocation-state-name' value={this.state.state} onChange={this.handleChange}>
                                <option value='Telangana'>Telangana</option>
                                {/* <option value='Andhra Pradesh'>Andhra Pradesh</option>
                                <option>Karnataka</option> */}
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='select-style'>
                              <select className='form-control' name='endusers-setlocation-city-name' value={this.state.city} onChange={this.handleChange}>
                                <option value='Hyderabad'>Hyderabad</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='select-style'>
                              <select className='form-control' name='endusers-setlocation-area-name' value={this.state.area} onChange={this.handleAreaChange}>
                                <option value=''>Select Area</option>
                                {locations.map((areas, i) =>
                                  <option value={areas.name} key={i} >{areas.name}</option>
                                )}
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='input-group'>
                              <span className='input-group-addon'><i className='fa fa-map-marker' aria-hidden='true' /></span>
                              <FormControl type='text' value={this.state.areaLocality} placeholder='Area Locality/Division' onChange={this.handleLocalityChange} minLength={3} maxLength={30} /></div></div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <div className='form-group'>
                            <div className='input-group'>
                              <span className='input-group-addon'><i className='fa fa-map-marker' aria-hidden='true' /></span>
                              <FormControl type='number' name='endusers-setlocation-zipcode-name' value={this.state.zip} placeholder='Pin Code'
                                onChange={this.handleChange} onKeyPress={this.handleNameNumKeys} maxLength={6} /></div></div>
                        </div>
                      </div>
                      <div className='text-center'>
                        <div className=''>
                          <div className='form-group'>
                            <div className=''>
                              {/* <span className='input-group-addon'><i className='fa fa-map-marker' aria-hidden='true' /></span> */}
                              <label className='warning' >{this.state.errorMessage}</label>
                              <br />
                              <Button bsStyle='info' onClick={this.getAddress}>Get Address </Button>&nbsp;&nbsp;
                              <Button bsStyle='primary' onClick={this.handleUpdate}> Update</Button>
                            </div></div></div></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    )
  }
}

export default EndUsersSetLocation
