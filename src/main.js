import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Route, hashHistory } from 'react-router'

import EndUsersHomeIndex from './components/endusers/home/index'
import EndUsersProductsPage from './components/endusers/home/products'
import EndUsersLogin from './components/endusers/login/login'
import EndUsersLoginOTP from './components/endusers/login/login_otp_validation'
import EndUsersSetLocation from './components/endusers/locations/set_delivery_location'
import EndUsersOrdersHome from './components/endusers/orders/home'
import EndUsersNormalOrdersAddToCart from './components/endusers/orders/normal_addto_cart'
import EndUsersOrdersIncartOrder from './components/endusers/orders/incart_order'
import EndUsersOrdersPaymentOptions from './components/endusers/orders/payment_options'
import EndUsersOrdersTrackerOrder from './components/endusers/orders/order_tracking'
import EndUsersOrderHistory from './components/endusers/orders-history/indesx'
import EndUsersProfile from './components/endusers/profile/index'
import ProductDetails from './components/endusers/orders/product_details'
import EndUserMyFavorites from './components/endusers/profile/Favorites'
import EndUserOrderHistoryView from './components/endusers/orders-history/order_historyview'
import EndUsersReview from './components/endusers/orders-history/Order_Review'
import EndUsersGetReviews from './components/endusers/profile/Reviews'
import EndUsersUpdateReviews from './components/endusers/orders-history/updateReview'
import EndUsersPaymentSuccess from './components/endusers/payments/success'
import EndUsersRegularOrderPaymentSuccess from './components/endusers/payments/regularorder_success'
import EndUsersPaymentFail from './components/endusers/payments/fail'
import EndUsersRegularOrderPaymentFail from './components/endusers/payments/regularorder_fail'
import EndUsersPaymentSetup from './components/endusers/payments/home'
import EndUsersRegularOrderPaymentSetup from './components/endusers/payments/regularorder_home'
import EndUsersMobilePaymentSetup from './components/endusers/payments/mobile_home'
import EndUsersMobileRegularOrderPaymentSetup from './components/endusers/payments/mobile_regularorder_home'
import EndUsersMobilePaymentSuccess from './components/endusers/payments/mobile_success'
import EndUsersMobileRegularOrderPaymentSuccess from './components/endusers/payments/mobile_regularorder_success'
import EndUsersMobilePaymentFail from './components/endusers/payments/mobile_fail'
import EndUsersMobileRegularOrderPaymentFail from './components/endusers/payments/mobile_regularorder_fail'
import EndUserRegularOrderHistory from './components/endusers/RegularOrdersHistory/indesx'
import EndUserChildRegularOrderHistory from './components/endusers/RegularOrdersHistory/regularchildorders'
import EndUserRegularOrderTracking from './components/endusers/RegularOrdersHistory/regularordertracking'
import EndUserChildRegularDeliveredOrderHistory from './components/endusers/RegularOrdersHistory/regulardeliveredchildorders'
import EndUserPlacedChildRegularOrderHistory from './components/endusers/RegularOrdersHistory/regularplacedorders'
import EndUserConfirmedChildRegularOrderHistory from './components/endusers/RegularOrdersHistory/regularconfirmchildorders'

import SupplierDashboard from './components/suppliers/home/dashboard'
import SupplierLogin from './components/suppliers/supplierlogin/login'
import SupplierSignup from './components/suppliers/supplierlogin/signup'
import SupplierOrdersPage from './components/suppliers/supplierOrders/orderspage'
import SuppliersetRegularOrder from './components/suppliers/supplierRegularOrders/setRegularOrder'

import SupplierRegularChildDeliveredOrdersPage from './components/suppliers/supplierRegularOrders/regularChildDeliveredOrders'

import SupplierCatalogue from './components/suppliers/suppliercatalog/supplier_catalog'
import SupplierAccounts from './components/suppliers/supplierAccount/acconts'
import SupplierSupport from './components/suppliers/suppliersupport/suppliersupport'
import SupplierVehicleDetails from './components/suppliers/supplierVehicleDetails/vehicledetails'
import SupplierWaterResource from './components/suppliers/supplierVehicleDetails/water-resource'
import SupplierRequestCouponPage from './components/suppliers/request-coupon/request-caupon'
import SupplierMyProfile from './components/suppliers/supplierProfile/supplier_profile'
import SupplierMyAddressBook from './components/suppliers/supplierProfile/supplier_address_book'
import SupplierPreferences from './components/suppliers/supplierProfile/supplier_preferences'
import SupplierSignupPreferences from './components/suppliers/supplierlogin/supplier-signup-preferences'
import SupplierProfileChangePassword from './components/suppliers/supplierProfile/supplier_Pro_Change_Pswd'
import SupplierLoginOTPscreen from './components/suppliers/supplierlogin/login-otp-validation'
import SupplierLoginForgotPassword from './components/suppliers/supplierlogin/login-reset-password-num'
import SupplierLoginForgotPasswordOTPScreen from './components/suppliers/supplierlogin/login-reset-password-otp'
import SupplierLoginResetPassword from './components/suppliers/supplierlogin/login-reset-password'

import SupplierNormalOrdersPage from './components/suppliers/supplierOrders/normalOrders'
import SupplierNormalOrdersConfirm from './components/suppliers/supplierOrders/toConfirmNormalOrder'
import SupplierNormalOrdersAssign from './components/suppliers/supplierOrders/toAssignNormalOrder'
import SupplierNormalOrdersDispatch from './components/suppliers/supplierOrders/toDispatchNormalOrder'
import SupplierNormalOrdersDeliver from './components/suppliers/supplierOrders/toDeliverNormalOrders'
import SupplierNormalOrdersOnHold from './components/suppliers/supplierOrders/toOnHoldNomalOrders'
import SupplierExpressOrdersConfirm from './components/suppliers/supplierOrders/toConfirmExpressOrder'
import SupplierExpressOrdersAssign from './components/suppliers/supplierOrders/toAssignExpressOrder'
import SupplierExpressOrdersDispatch from './components/suppliers/supplierOrders/toDispatchExpressOrder'
import SupplierExpressOrdersDeliver from './components/suppliers/supplierOrders/toDeliverExpressOrders'
import SupplierExpressOrdersOnHold from './components/suppliers/supplierOrders/toOnHoldExpressOrders'
import SupplierReviews from './components/suppliers/supplierreviews/reviews'
import SupplierConfirmOrdersPage from './components/suppliers/supplierOrders/confirmOrdersPage'
import SupplierAssignedOrdersPage from './components/suppliers/supplierOrders/assignedOrdersPage'
import SupplierDispatchedOrdersPage from './components/suppliers/supplierOrders/dispatchOrderspage'
import SupplierOnHoldOrdersPage from './components/suppliers/supplierOrders/onHoldOrderspage'
import SupplierCancelledOrdersPage from './components/suppliers/supplierOrders/cancelledOrdersPage'
import SupplierBlockedOrdersPage from './components/suppliers/supplierOrders/blockedOrdersPage'
import SupplierSetRegularOrdersPageView from './components/suppliers/supplierRegularOrders/setRegularOrderView'
// import SuppliersetRegularAssignOrder from './components/suppliers/supplierRegularOrders/setRegularOrdeOrder'
import SuppliersetRegularChildDispatchOrder from './components/suppliers/supplierRegularOrders/setRegularChildDispatchOrder'
import SuppliersetRegularChildOnHoldOrder from './components/suppliers/supplierRegularOrders/setRegularChildOnHoldOrder'
import SuppliersetRegularChildDeliverOrder from './components/suppliers/supplierRegularOrders/setRegularChildDeliverOrder'
import SuppliersetRegularChildRestartOrder from './components/suppliers/supplierRegularOrders/setRegularChildRestartOrder'
import SuppliersetRegularChildCancelOrder from './components/suppliers/supplierRegularOrders/setRegularChildCancelOrder'
import SupplierRegularConfirmOrders from './components/suppliers/supplierRegularOrders/regularOrderConfirm'

import SupplierNewVehicleDetails from './components/suppliers/supplierVehicleDetails/vehiclenew'
import UpdateSupplierNewVehicleDetails from './components/suppliers/supplierVehicleDetails/updateSupplierVehicleDetails'
import SupplierDriverDetails from './components/suppliers/supplierVehicleDetails/supplierDriverDetails'
import SupplierUpdateDriver from './components/suppliers/supplierVehicleDetails/updateDriverDetails'
import SupplierEditCatalogue from './components/suppliers/suppliercatalog/supplier_catalog_edit'
import SupplierRejectOrdersPage from './components/suppliers/supplierOrders/rejectedOrdersPage'
import SupplierExpressOrdersPage from './components/suppliers/supplierOrders/expressorders'
import SupplierDeliverOrdersPage from './components/suppliers/supplierOrders/deliverOrders'
import SupplierDeliveryZones from './components/suppliers/servicelocations/index'
import UpdateSupplierDeliveryZones from './components/suppliers/servicelocations/updateDeliveryZone'
import SupplierResourcesDetails from './components/suppliers/supplierresources/supplierresourcesdetails'
import SupplierUpdateResources from './components/suppliers/supplierresources/updateresourcesdetails'
import SupplierActivateOTPscreen from './components/suppliers/supplierlogin/activate_user'
import DeleteSupplierNewVehicleDetails from './components/suppliers/supplierVehicleDetails/deleteSupplierVehicleDetails'

import AdminLogin from './components/admin/Login/login'
import AdminLoginForgotPassword from './components/admin/Login/adminlogin-reset-password-num'
import AdminLoginForgotPasswordOTPScreen from './components/admin/Login/adminlogin-reset-password-otp'
import AdminLoginResetPassword from './components/admin/Login/resetPassword'
import AdminDashboard from './components/admin/home/dashboard'
import AdminAccounts from './components/admin/adminAccount/acconts'
import AdminNormalOrders from './components/admin/normalOrders/normalOrders'
import AdminExpressOrders from './components/admin/expressOrders/expressOrders'
import AdminRegularOrders from './components/admin/regularOrders/regularOrders'
import AdminCustomers from './components/admin/customers/customers'
import UpdateCustomers from './components/admin/customers/updatecustomers'
import AdminCustomerFavourites from './components/admin/customers/customersfavourites'
import AdminCustomerLocationList from './components/admin/customers/locationslist'
import AdminCustomerLocationUpdate from './components/admin/customers/location_update'
import Suppliers from './components/admin/suppliers/suppliers'
import AdminEditServiceProvider from './components/admin/suppliers/suppliers_edit'
import AdminEnquiries from './components/admin/enquiries/enquiries'
import Reviews from './components/admin/Reviews/reviews'
import AdminReviewUpdate from './components/admin/Reviews/review_update'
import Support from './components/admin/Support/support'
import handleSupportview from './components/admin/Support/support_view'
import AdminProfile from './components/admin/Profile/profile'
import ProfileChangePassword from './components/admin/Profile/changepassword'
import AdminUser from './components/admin/AdminUser/adminUser'
import AdminUpdateAdminUser from './components/admin/AdminUser/adminUser_update'
import AdminNormalOrdersConfirm from './components/admin/normalOrders/toConfirmNormalOrders'
import AdminNormalOrdersAssign from './components/admin/normalOrders/toAssignNormalOrders'
import AdminRegularOrderConfirm from './components/admin/regularOrders/confirmRegularOrderPage'
import AdminSetRegularOrdersPageView from './components/admin/regularOrders/regularChildOrders'
import AdminRegularChildDispatchOrder from './components/admin/regularOrders/regularChildStartOrder'
import AdminRegularChildDeliveredOrders from './components/admin/regularOrders/deliveredRegularChildOrders'
import AdminRegularChildDeliverOrder from './components/admin/regularOrders/regularChildDeliverOrder'
import AdminRegularChildOnHoldOrder from './components/admin/regularOrders/regularChildCancleOrder'
import AdminSupplierResourcesList from './components/admin/suppliers/resourceslist'
import AdminResourceUpdate from './components/admin/suppliers/resource_update'
import AdminSupplierservicesList from './components/admin/suppliers/serviceslist'
import AdminServiceUpdate from './components/admin/suppliers/services_update'
import AdminSupplierDeliveryArea from './components/admin/suppliers/deliveryareas'
import AdminDeliveryAreaUpdate from './components/admin/suppliers/deliver_area_update'
import AdminSupplierFavorites from './components/admin/suppliers/favorites'
import AdminSupplierVehicleList from './components/admin/suppliers/vehiclelist'
import AdminSupplierVehicleUpdate from './components/admin/suppliers/vehicle_update'
// import AdminRegularChildDeliveredOrderView from './components/admin/regularOrders/deliveredChildOrderView'
import AdminRegularRejectedOrder from './components/admin/regularOrders/rejectedRegularOrderView'
import AdminRegularChildCompletedOrderView from './components/admin/regularOrders/deliveredRegularChildOrderView'
import AdminNormalOrdersDispatch from './components/admin/normalOrders/toDispatchNormalOrders'
import AdminNormalOrdersOnHold from './components/admin/normalOrders/toOnHoldNormalOrders'
import AdminNormalOrdersDeliver from './components/admin/normalOrders/toDeliverNormalOrders'
// import AdmincustomerLocations from './components/admin/customers/customerLocations'
import AdminRegularChildBlockedOrder from './components/admin/regularOrders/regularChildBlockedOrder'
import AdminExpressOrdersConfirm from './components/admin/expressOrders/toConfirmExpressOrders'
import AdminExpressOrdersAssign from './components/admin/expressOrders/toAssignExpressOrders'
import AdminExpressOrdersDispatch from './components/admin/expressOrders/toDispatchExpressOrders'
import AdminExpressOrdersDeliver from './components/admin/expressOrders/toDeliverExpressOrders'
import AdminExpressOrdersOnHold from './components/admin/expressOrders/toOnHoldExpressOrders'
import AdminSupportAssign from './components/admin/Support/supportAssign'
import AdminSupportStart from './components/admin/Support/SupportStart'
import AdminSupportResolve from './components/admin/Support/supportResolve'
import AdminSupplierDeleteVehicleDetails from './components/admin/suppliers/todeleteVehicle'
import AdminSupportComplete from './components/admin/Support/supportComplete'
import AdminEnquiryView from './components/admin/enquiries/enquiryView'
import AdminEnquiryReply from './components/admin/enquiries/enquiryReply'
import AdminReview from './components/admin/Reviews/review_view'
import AdminRegularOrderEdit from './components/admin/regularOrders/editRegularOrder'
import AdminEditNormalOrders from './components/admin/normalOrders/toEditNormalOrders'
import AdminExpressOrdersEdit from './components/admin/expressOrders/toEditExpressOrders'

require('../node_modules/jquery/dist/jquery.js')
require('../node_modules/jquery-ui-dist/jquery-ui.js')
require('es6-promise/auto')
require('../node_modules/bootstrap/dist/css/bootstrap.min.css')
require('../node_modules/font-awesome/css/font-awesome.css')
require('../node_modules/lato-font/css/lato-font.css')
require('../node_modules/bootstrap/dist/js/bootstrap.min.js')
require('../node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')
require('../node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')
require('../node_modules/owl-slider/js/owl.carousel.js')
require('../node_modules/owl-slider/css/owl.carousel.css')
require('../node_modules/owl-slider/css/owl.theme.default.css')
require('../node_modules/rangeslider.js/dist/rangeslider.js')
require('js/wb-custom.js')
// require('css/wb-plugins.css')
require('css/my_profile.css')
require('css/wb-styles.css')
// require('css/wb-responsive-enduser.css')
require('css/style.css')
require('css/login_signup.css')
require('css/locations.css')
require('../src/components/suppliers/css/responsive.css')

// ========================================================
// Store Instantiation
// ========================================================
// const initialState = window.___INITIAL_STATE__
// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root')

// const checkLoggedIn = (nextState, replace) => {
//   /* if (localStorage.getItem('authobj')) {
//     replace({
//       pathname: '/prelogin'
//     })
//   } */
// }
const checkLoggedIn = (nextState, replace) => {
  let authObj = JSON.parse(localStorage.getItem('authObj'))
  if (authObj && authObj.entityType !== 'Customer') {
    hashHistory.push('/login')
  }
}
const checkSupplierLoggedIn = (nextState, replace) => {
  let authObj = JSON.parse(localStorage.getItem('authObj'))
  if (authObj && authObj.entityType !== 'Service Provider') {
    hashHistory.push('/supplierlogin')
  }
}
const checkAdminLoggedIn = (nextState, replace) => {
  let authObj = JSON.parse(localStorage.getItem('authObj'))
  if (authObj && authObj.entityType !== 'Admin') {
    hashHistory.push('/admin')
  }
}

// window.onhashchange = segmentCallManager.logPageChange
// window.onload = segmentCallManager.logInitialLoad

let render = () => {
  ReactDOM.render(
    <Provider>
      <Router history={hashHistory}>
        <Route components={EndUsersHomeIndex} path='/' >
          <Route components={EndUsersProductsPage} path='/products' onEnter={checkLoggedIn} />
        </Route>
        <Route components={ProductDetails} path='/product/details' onEnter={checkLoggedIn} />
        <Route components={EndUsersLogin} handler={EndUsersLogin} path='/login' />
        <Route components={EndUsersLoginOTP} path='/login/otp' />
        <Route components={EndUsersSetLocation} path='/locations/set' />
        <Route components={EndUsersOrdersHome} path='/orders' />
        <Route components={EndUsersNormalOrdersAddToCart} path='/order/edit-order' onEnter={checkLoggedIn} />
        <Route components={EndUsersOrdersIncartOrder} path='/order/incart' />
        <Route components={EndUsersOrdersPaymentOptions} path='/order/payment' onEnter={checkLoggedIn} />
        <Route components={EndUsersOrdersTrackerOrder} path='/order/tracking' />
        <Route components={EndUsersOrderHistory} path='/orders-history' onEnter={checkLoggedIn} />
        <Route components={EndUsersProfile} path='/profile' onEnter={checkLoggedIn} />
        <Route components={EndUserMyFavorites} path='/favorites' onEnter={checkLoggedIn} />
        <Route components={EndUserOrderHistoryView} path='/order/historyview' onEnter={checkLoggedIn} />
        <Route components={EndUsersReview} path='/order/review' onEnter={checkLoggedIn} />
        <Route components={EndUsersGetReviews} path='/reviews' onEnter={checkLoggedIn} />
        <Route components={EndUsersUpdateReviews} path='/order/updatereview' onEnter={checkLoggedIn} />
        <Route components={EndUsersPaymentSuccess} path='/payment/success/:id' onEnter={checkLoggedIn} />
        <Route components={EndUsersRegularOrderPaymentSuccess} path='/regular-order/payment/success/:id' onEnter={checkLoggedIn} />
        <Route components={EndUsersPaymentFail} path='/payment/fail/:id' onEnter={checkLoggedIn} />
        <Route components={EndUsersRegularOrderPaymentFail} path='/regular-order/payment/fail/:id' onEnter={checkLoggedIn} />
        <Route components={EndUsersPaymentSetup} path='/order/payment/setup' />
        <Route components={EndUsersRegularOrderPaymentSetup} path='/order/regular/payment/setup' onEnter={checkLoggedIn} />
        <Route components={EndUsersMobilePaymentSetup} path='/payment/setup/:id' />
        <Route components={EndUsersMobileRegularOrderPaymentSetup} path='/regular-order/payment/setup/:id' />
        <Route components={EndUsersMobilePaymentSuccess} path='/mobile/payment/success/:id' />
        <Route components={EndUsersMobileRegularOrderPaymentSuccess} path='/mobile/regular-order/payment/success/:id' />
        <Route components={EndUsersMobilePaymentFail} path='/mobile/payment/fail/:id' />
        <Route components={EndUsersMobileRegularOrderPaymentFail} path='/mobile/regular-order/payment/fail/:id' />
        <Route components={EndUserRegularOrderHistory} path='/regularorders-history' onEnter={checkLoggedIn} />
        <Route components={EndUserChildRegularOrderHistory} path='/regularchildorders-history' onEnter={checkLoggedIn} />
        <Route components={EndUserRegularOrderTracking} path='/regularordertracking' onEnter={checkLoggedIn} />
        <Route components={EndUserChildRegularDeliveredOrderHistory} path='/regulardeliveredorders' onEnter={checkLoggedIn} />
        <Route components={EndUserPlacedChildRegularOrderHistory} path='/regularplacedchildorders-history' onEnter={checkLoggedIn} />
        <Route components={EndUserConfirmedChildRegularOrderHistory} path='/regularconfirmedchildorders-history' onEnter={checkLoggedIn} />

        <Route component={SupplierLogin} path='/supplierlogin' />
        <Route component={SupplierSignup} path='/suppliersignup' />
        <Route component={SupplierLoginOTPscreen} path='/supplierloginotp' />
        <Route component={SupplierActivateOTPscreen} path='/supplier/activation' />
        <Route component={SupplierLoginForgotPassword} path='/supplierlogin/forgotpassword' />
        <Route component={SupplierLoginForgotPasswordOTPScreen} path='/supplierlogin/forgotpassword/otp' />
        <Route component={SupplierLoginForgotPasswordOTPScreen} path='/supplieraccount/activation/otp' />
        <Route component={SupplierLoginResetPassword} path='/supplierlogin/password/reset' />
        <Route components={SupplierDashboard} path='/supplierdashboard' onEnter={checkSupplierLoggedIn} />

        <Route component={SupplierOrdersPage} path='/supplierorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SuppliersetRegularOrder} path='/suppliersetRegularOrder' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierRegularChildDeliveredOrdersPage} path='/supplierRegularChildOrders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierRegularConfirmOrders} path='/supplierRegularConfirmOrders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierNormalOrdersPage} path='/suppliernormalorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierNormalOrdersConfirm} path='/supplier/normalorders/confirm' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierNormalOrdersAssign} path='/supplier/normalorders/assign' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierNormalOrdersDispatch} path='/supplier/normalorders/dispatch' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierNormalOrdersDeliver} path='/supplier/normalorders/deliver' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierNormalOrdersOnHold} path='/supplier/normalorders/onhold' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierSetRegularOrdersPageView} path='/suppliersetRegularOrderView' onEnter={checkSupplierLoggedIn} />
        {/* <Route component={SuppliersetRegularAssignOrder} path='/suppliersetRegularAssignOrder' onEnter={checkLoggedIn} /> */}
        <Route component={SuppliersetRegularChildDispatchOrder} path='/suppliersetRegularChildDispatchOrder' onEnter={checkSupplierLoggedIn} />
        <Route component={SuppliersetRegularChildOnHoldOrder} path='/suppliersetRegularChildOnHoldOrder' onEnter={checkSupplierLoggedIn} />
        <Route component={SuppliersetRegularChildDeliverOrder} path='/suppliersetRegularChildDeliverOrder' onEnter={checkSupplierLoggedIn} />
        <Route component={SuppliersetRegularChildRestartOrder} path='/suppliersetRegularChildRestartOrder' onEnter={checkSupplierLoggedIn} />
        <Route component={SuppliersetRegularChildCancelOrder} path='/suppliersetRegularChildCancelOrder' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierExpressOrdersConfirm} path='/supplier/expressorders/confirm' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierExpressOrdersAssign} path='/supplier/expressorders/assign' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierExpressOrdersDispatch} path='/supplier/expressorders/dispatch' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierExpressOrdersDeliver} path='/supplier/expressorders/deliver' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierExpressOrdersOnHold} path='/supplier/expressorders/onhold' onEnter={checkSupplierLoggedIn} />

        <Route component={SupplierCatalogue} path='/suppliercatalog' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierAccounts} path='/supplieraccount' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierSupport} path='/suppliersupport' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierVehicleDetails} path='/suppliervehicledetails' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierWaterResource} path='/supplierwater-resource' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierRequestCouponPage} path='/supplierreq-coupon' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierReviews} path='/supplierreview' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierMyProfile} path='/supplierprofile' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierMyAddressBook} path='/myaddress' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierPreferences} path='/preferences' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierSignupPreferences} path='/signup/preferences' />
        <Route component={SupplierProfileChangePassword} path='/changepassword' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierConfirmOrdersPage} path='/supplierconfirmorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierAssignedOrdersPage} path='/supplierassignedorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierDispatchedOrdersPage} path='/supplierdispatchedorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierOnHoldOrdersPage} path='/supplieronholdorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierCancelledOrdersPage} path='/suppliercancelledorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierBlockedOrdersPage} path='/supplierblockedorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierExpressOrdersPage} path='/supplierexpressorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierNewVehicleDetails} path='/suppliernewvehicledetails' onEnter={checkSupplierLoggedIn} />
        <Route component={UpdateSupplierNewVehicleDetails} path='/updatesuppliervehicledetails' onEnter={checkSupplierLoggedIn} />
        <Route component={DeleteSupplierNewVehicleDetails} path='/deletesuppliervehicledetails' onEnter={checkSupplierLoggedIn} />

        <Route component={SupplierDriverDetails} path='/supplierdriverdetails' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierUpdateDriver} path='/supplierupdatedriver' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierEditCatalogue} path='/suppliereditcatalog' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierRejectOrdersPage} path='/supplierrejectorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierDeliverOrdersPage} path='/supplierdeliveredorders' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierDeliveryZones} path='/supplier/deliveryzones' onEnter={checkSupplierLoggedIn} />
        <Route component={UpdateSupplierDeliveryZones} path='/supplier/deliveryzones/update' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierResourcesDetails} path='/supplierresources' onEnter={checkSupplierLoggedIn} />
        <Route component={SupplierUpdateResources} path='/updatesupplierresources' onEnter={checkSupplierLoggedIn} />

        <Route component={AdminLogin} path='/admin' />
        <Route component={AdminLogin} path='/admin/login' />
        <Route component={AdminLoginForgotPassword} path='/admin/login/forgotpassword' />
        <Route component={AdminLoginForgotPasswordOTPScreen} path='/admin/login/forgotpassword/otp' />
        <Route component={AdminLoginResetPassword} path='/admin/login/reset' />
        <Route component={AdminDashboard} path='/admin/dashboard' onEnter={checkAdminLoggedIn} />
        <Route component={AdminAccounts} path='/admin/accounts' onEnter={checkAdminLoggedIn} />
        <Route component={AdminNormalOrders} path='/admin/normalorders' onEnter={checkAdminLoggedIn} />
        <Route component={AdminNormalOrdersDispatch} path='/admin/normalorders/dispatch' onEnter={checkAdminLoggedIn} />
        <Route component={AdminNormalOrdersDeliver} path='/admin/normalorders/deliver' onEnter={checkAdminLoggedIn} />
        <Route component={AdminNormalOrdersOnHold} path='/admin/normalorders/onhold' onEnter={checkAdminLoggedIn} />
        <Route component={AdminExpressOrders} path='/admin/expressorders' onEnter={checkAdminLoggedIn} />
        <Route component={AdminExpressOrdersAssign} path='/admin/expressorders/assign' onEnter={checkAdminLoggedIn} />
        <Route component={AdminExpressOrdersDispatch} path='/admin/expressorders/dispatch' onEnter={checkAdminLoggedIn} />
        <Route component={AdminExpressOrdersDeliver} path='/admin/expressorders/deliver' onEnter={checkAdminLoggedIn} />
        <Route component={AdminExpressOrdersOnHold} path='/admin/expressorders/onhold' onEnter={checkAdminLoggedIn} />

        <Route component={AdminRegularOrders} path='/admin/regularorders' onEnter={checkAdminLoggedIn} />
        <Route component={AdminCustomers} path='/admin/admincustomers' onEnter={checkAdminLoggedIn} />
        <Route component={UpdateCustomers} path='/admin/updatecustomers' onEnter={checkAdminLoggedIn} />
        <Route component={AdminCustomerFavourites} path='/admin/customersfav' onEnter={checkAdminLoggedIn} />
        <Route component={AdminCustomerLocationList} path='/admin/locationlist' onEnter={checkAdminLoggedIn} />
        <Route component={AdminCustomerLocationUpdate} path='/admin/location_update' onEnter={checkAdminLoggedIn} />
        {/* <Route component={AdmincustomerLocations} path='/admin/customerlocations' onEnter={checkLoggedIn} /> */}
        <Route component={Suppliers} path='/admin/suppliers' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupplierResourcesList} path='/admin/supplier/resource' onEnter={checkAdminLoggedIn} />
        <Route component={AdminResourceUpdate} path='/admin/supplier/resource_update' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupplierservicesList} path='/admin/supplier/service' onEnter={checkAdminLoggedIn} />
        <Route component={AdminServiceUpdate} path='/admin/supplier/service_update' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupplierDeliveryArea} path='/admin/supplier/service/deliveryarea' onEnter={checkAdminLoggedIn} />
        <Route component={AdminDeliveryAreaUpdate} path='/admin/supplier/service/deliver_area_update' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupplierFavorites} path='admin/supplier/favourites' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupplierVehicleList} path='admin/supplier/vehicles' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupplierDeleteVehicleDetails} path='admin/supplier/deletevehicle' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupplierVehicleUpdate} path='/admin/supplier/updatevehicle' onEnter={checkAdminLoggedIn} />
        <Route component={AdminEditServiceProvider} path='/admin/supplier_update' onEnter={checkAdminLoggedIn} />
        <Route component={AdminEnquiries} path='/admin/enquiry' onEnter={checkAdminLoggedIn} />
        <Route component={Reviews} path='/admin/review' onEnter={checkAdminLoggedIn} />
        <Route component={AdminReviewUpdate} path='/admin/review_update' onEnter={checkAdminLoggedIn} />
        <Route component={Support} path='/admin/support' onEnter={checkAdminLoggedIn} />
        <Route component={handleSupportview} path='/admin/support_view' onEnter={checkAdminLoggedIn} />
        <Route component={AdminProfile} path='/admin/profile' onEnter={checkAdminLoggedIn} />
        <Route component={ProfileChangePassword} path='/admin/changepassword' onEnter={checkAdminLoggedIn} />
        <Route component={AdminUser} path='/admin/adminuser' onEnter={checkAdminLoggedIn} />
        <Route component={AdminUpdateAdminUser} path='/admin/update_adminuser' onEnter={checkAdminLoggedIn} />
        <Route component={AdminNormalOrdersConfirm} path='/admin/normalorders/confirm' onEnter={checkAdminLoggedIn} />
        <Route component={AdminExpressOrdersConfirm} path='/admin/expressorders/confirm' onEnter={checkAdminLoggedIn} />
        <Route component={AdminNormalOrdersAssign} path='/admin/normalorders/assign' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularOrderConfirm} path='/admin/confirmregularorder' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSetRegularOrdersPageView} path='/admin/childorders' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularChildDispatchOrder} path='/admin/childstartorder' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularChildDeliveredOrders} path='/admin/childdeliveredorders' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularChildDeliverOrder} path='/admin/deliverchildorder' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularChildOnHoldOrder} path='/admin/onholdchildorder' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularChildBlockedOrder} path='/admin/childblockedorder' onEnter={checkAdminLoggedIn} />
        {/* <Route component={AdminRegularChildDeliveredOrderView} path='/admin/childeliverview' onEnter={checkAdminLoggedIn} /> */}
        <Route component={AdminRegularRejectedOrder} path='/admin/rejectedorders' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularChildCompletedOrderView} path='/admin/child/completedorders' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupportAssign} path='/admin/support_assign' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupportStart} path='/admin/support_start' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupportResolve} path='/admin/support_resolve' onEnter={checkAdminLoggedIn} />
        <Route component={AdminSupportComplete} path='/admin/support_complete' onEnter={checkAdminLoggedIn} />
        <Route component={AdminEnquiryView} path='/admin/enquirypage' onEnter={checkAdminLoggedIn} />
        <Route component={AdminEnquiryReply} path='/admin/enquiryreply' onEnter={checkAdminLoggedIn} />
        <Route component={AdminReview} path='/admin/review_view' onEnter={checkAdminLoggedIn} />
        <Route component={AdminRegularOrderEdit} path='/admin/edit_order' onEnter={checkAdminLoggedIn} />
        <Route component={AdminEditNormalOrders} path='/admin/normalorders/edit' onEnter={checkAdminLoggedIn} />
        <Route component={AdminExpressOrdersEdit} path='/admin/expressorders/edit' onEnter={checkAdminLoggedIn} />
      </Router>
    </Provider>,
    MOUNT_NODE)
}

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderApp = render
    const renderError = (error) => {
      const RedBox = require('redbox-react').default

      ReactDOM.render(
        <RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp()
      } catch (error) {
        // console.error(error)
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./components/endusers/login/login', () => setImmediate(() => {
      ReactDOM.unmountComponentAtNode(MOUNT_NODE)
      render()
    }))
  }
}

// ========================================================
// Go!
// ========================================================
render()
